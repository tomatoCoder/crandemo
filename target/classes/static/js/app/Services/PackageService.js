'use strict'

angular.module('demo.packageService', []).factory('PackageService',
    [ "$http", "CONSTANTS", function($http, CONSTANTS) {
        var service = {};
        service.getAllPackages = function() {
            return $http.get(CONSTANTS.getAllPackages);
        }
        service.getPackgesByAuthor = function(author) {
            var url = CONSTANTS.getPackgesByAuthor + author;
            return $http.get(url);
        }
        return service;
    } ]);