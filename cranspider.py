#!/usr/bin/env python
import json
import requests
import tarfile
import urllib2

url1 = 'https://cran.r-project.org/src/contrib/PACKAGES'
f = urllib2.urlopen(url1)
packages1 = f.read()
packages = packages1.splitlines()
urlList = []
for packageDetails in packages:
	if 'Package' in packageDetails:
		package = packageDetails.split(":", 1)[1]
	if 'Version' in packageDetails:
		version = packageDetails.split(":", 1)[1]
		urlList.append(package.strip() + "_"+version.strip())


for idx in range(1, 500):
	try:
		url = 'https://ftp.iitm.ac.in/cran/src/contrib/' + urlList[idx] + ".tar.gz"
		idx += 1
		print url
		f1 = urllib2.urlopen(url)
		with open("code.tar.gz", "wb") as code:
			code.write(f1.read())

		tar = tarfile.open("code.tar.gz", "r:gz")
		tar.extractall()
		tar.close()
		for tarinfo in tar:
			if "/DESCRIPTION" in tarinfo.name:
				data1= open(tarinfo.name).read().splitlines()

		#Cleaning up data
		data = []
		i = 0
		while i < len(data1)-1:
			i += 1
			if ':' not in data1[i]:
				data1[i-1] += data1[i]

		for line in data1:
			if ':' in line:
				data.append(line)


		dataDict = {line.split(":",1)[0]:line.split(":",1)[1] for line in data}
		print dataDict
		payloadDict = {}
		payloadDict['packageDetailsName'] = dataDict['Package']
		payloadDict['packageDetailsVersion'] = dataDict['Version']
		payloadDict['packageDescription'] = dataDict['Description']
		payloadDict['packageLicense'] = dataDict['License']
		payloadDict['dependencies'] = dataDict['Depends']
		payloadDict['authorName'] = dataDict['Author']
		payloadDict['authorEmail'] = dataDict['Maintainer']

		localhostUrl="http://localhost:8080/package/savePackage"
		payloadJson = json.dumps(payloadDict)
		headers = {'Content-type': 'application/json', 'Accept': 'application/json'}
		r = requests.post(localhostUrl,payloadJson,headers=headers)
		print r.text
	
	except:
		continue


		