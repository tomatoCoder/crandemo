# README #

This project is built to provide UI for exploring packages in a CRAN repository. 

MVC architecture has been used in Backend as well as frontend.  

### What is this repository for? ###
* Version
0.0.1

### How do I get set up? ###

Clone repository and open it up in your favourite IDE.
### Dependencies ###
This project is based on Java8, Python2 and Javascript(AngularJS, Bootstrap)

1. Make sure that you have Java8 installed and a compatible JRE to run
2. Install Python 2.7.10
3. Create virtual environment for python2 (Optional)
4. Install python 'requests' module using 'sudo pip install requests'

### Database configuration ###
1. Open src -> main -> java -> resource -> application.propertiesand edit following according to your settings

	`spring.datasource.url=jdbc:mysql://localhost:3306/cran?useSSL=false`
	
	`spring.datasource.username=root`
	
	`spring.datasource.password= `


2. Go to mysql server and create schema 'cran' using 'CREATE DATABASE CRAN'

### Deployment instructions ###
1. Go to `crandemo` directory and build using `mvn clean install`
2. Go to `target` folder and hit `java -jar bootdemo-0.0.1-SNAPSHOT.jar`
3. Go to a terminal tab
4. Activate your virtual environment (optional)
5. Go to directory where `cranSpider.py` exists.
6. (Optional)Change values in `Range(,)` function on `line 20` to change number of packages to be put in your db.
7. Make it executable using `chmod +x cranSpider.py`
8. Run it to fill up data in DB. (Needs to be done only once)
9. Open browser and go to 'http://localhost:8080/home'
10. Explore CRAN repository

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
sanglevikas25@gmail.com
* Other community or team contact