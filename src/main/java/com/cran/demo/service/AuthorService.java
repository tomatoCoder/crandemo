package com.cran.demo.service;

import com.cran.demo.dto.PackageDetailsDto;

import java.util.List;

public interface AuthorService {
    List<PackageDetailsDto> findPackagesByAuthor(String authorName);
}
