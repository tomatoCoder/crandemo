package com.cran.demo.service.impl;

import com.cran.demo.converter.AuthorConverter;
import com.cran.demo.converter.PackageDetailsConverter;
import com.cran.demo.dto.AuthorDto;
import com.cran.demo.dto.PackageDetailsDto;
import com.cran.demo.repository.PackageDetailsRepository;
import com.cran.demo.service.PackageDetailsService;
import com.cran.demo.repository.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PackageDetailsDetailsServiceImpl implements PackageDetailsService {

    @Autowired
    AuthorRepository authorRepository;

    @Autowired
    PackageDetailsRepository packageDetailsRepository;

    public AuthorRepository getAuthorRepository() {
        return authorRepository;
    }

    public void setAuthorRepository(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    public PackageDetailsRepository getPackageDetailsRepository() {
        return packageDetailsRepository;
    }

    public void setPackageDetailsRepository(PackageDetailsRepository packageDetailsRepository) {
        this.packageDetailsRepository = packageDetailsRepository;
    }

    @Override
    public void savePackage(PackageDetailsDto packageDetailsDto) throws Exception {
        if(packageDetailsRepository.findFirstByPackageDetailsNameAndPackageDetailsVersion(packageDetailsDto.getPackageDetailsName(),packageDetailsDto.getPackageDetailsVersion())==null){
            if (authorRepository.findAuthorByAuthorEmail(packageDetailsDto.getAuthorEmail()) == null) {
                authorRepository.save(AuthorConverter.dtoToEntity(new AuthorDto(packageDetailsDto.getAuthorEmail(), packageDetailsDto.getAuthorName())));
            }
            packageDetailsDto.setAuthorId(authorRepository.findAuthorByAuthorEmail(packageDetailsDto.getAuthorEmail()).getAuthorId());
            packageDetailsRepository.save(PackageDetailsConverter.dtoToEntity(packageDetailsDto));
        }
        else{
            throw new Exception("Package Already Exists");
        }
    }

    @Override
    public PackageDetailsDto getPackageDetailsById(Integer packageDetailsId) {
        PackageDetailsDto packageDetailsDto =  PackageDetailsConverter.entityToDto(packageDetailsRepository.findFirstByPackageDetailsId(packageDetailsId));
        packageDetailsDto.setAuthorEmail(authorRepository.findOne(packageDetailsDto.getAuthorId()).getAuthorEmail());
        packageDetailsDto.setAuthorName(authorRepository.findOne(packageDetailsDto.getAuthorId()).getAuthorName());
        return packageDetailsDto;
    }

    @Override
    public PackageDetailsDto getPackageDetailsByName(String packageDetailsName) {
        return PackageDetailsConverter.entityToDto(packageDetailsRepository.findFirstByPackageDetailsName(packageDetailsName));
    }

    @Override
    public List<PackageDetailsDto> getAllPackages() {
        List<PackageDetailsDto> packageDetailsDtoList = packageDetailsRepository.findAll().stream().map(PackageDetailsConverter::entityToDto).collect(Collectors.toList());
        packageDetailsDtoList.forEach(entry->{
            entry.setAuthorEmail(authorRepository.findOne(entry.getAuthorId()).getAuthorEmail());
            entry.setAuthorName(authorRepository.findOne(entry.getAuthorId()).getAuthorName());
        });
        return packageDetailsDtoList;
    }
}
