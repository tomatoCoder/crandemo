package com.cran.demo.service.impl;

import com.cran.demo.converter.PackageDetailsConverter;
import com.cran.demo.dto.PackageDetailsDto;
import com.cran.demo.entity.Author;
import com.cran.demo.repository.AuthorRepository;
import com.cran.demo.repository.PackageDetailsRepository;
import com.cran.demo.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AuthorServiceImpl implements AuthorService {

    @Autowired
    AuthorRepository authorRepository;

    @Autowired
    PackageDetailsRepository packageDetailsRepository;

    @Override
    public List<PackageDetailsDto> findPackagesByAuthor(String authorName) {
        List<Author> authorList = new ArrayList<>();
        List<PackageDetailsDto> packageDetailsDtos = new ArrayList<>();
        authorList = authorRepository.findAllByAuthorNameIsContaining(authorName);
        for(Author author: authorList){
            packageDetailsDtos.addAll(packageDetailsRepository.findAllByAuthorId(author.getAuthorId()).stream().map(PackageDetailsConverter::entityToDto).collect(Collectors.toList()));
            packageDetailsDtos.forEach(entry->{
                entry.setAuthorEmail(authorRepository.findOne(entry.getAuthorId()).getAuthorEmail());
                entry.setAuthorName(authorRepository.findOne(entry.getAuthorId()).getAuthorName());
            });
        }
        return packageDetailsDtos;
    }
}
