package com.cran.demo.service;

import com.cran.demo.dto.PackageDetailsDto;

import java.util.List;

public interface PackageDetailsService {
    PackageDetailsDto getPackageDetailsById(Integer packageDetailsId);
    PackageDetailsDto getPackageDetailsByName(String packageDetailsName);
    void savePackage(PackageDetailsDto packageDetailsDto) throws Exception;
    List<PackageDetailsDto> getAllPackages();
}
