package com.cran.demo.converter;

import com.cran.demo.entity.Author;
import com.cran.demo.dto.AuthorDto;

public class AuthorConverter {
    public static Author dtoToEntity(AuthorDto authorDto) {
        Author author = new Author(authorDto.getAuthorEmail(),authorDto.getAuthorName());
        author.setAuthorId(authorDto.getAuthorId());
        return author;
    }

    public static AuthorDto entityToDto(Author author) {
        AuthorDto authorDto = new AuthorDto(author.getAuthorEmail(),author.getAuthorName());
        authorDto.setAuthorId(author.getAuthorId());
        return authorDto;
    }
}
