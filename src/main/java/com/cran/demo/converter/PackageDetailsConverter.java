package com.cran.demo.converter;

import com.cran.demo.dto.PackageDetailsDto;
import com.cran.demo.entity.PackageDetails;
import com.cran.demo.repository.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class PackageDetailsConverter {

    @Autowired
    AuthorRepository authorRepository;

    public AuthorRepository getAuthorRepository() {
        return authorRepository;
    }

    public void setAuthorRepository(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    public static PackageDetails dtoToEntity(PackageDetailsDto packageDetailsDto) {
        PackageDetails packageDetails = new PackageDetails(packageDetailsDto.getPackageDetailsName(),packageDetailsDto.getPackageDescription(),packageDetailsDto.getPackageDetailsVersion(), packageDetailsDto.getAuthorId(),packageDetailsDto.getPackageLicense(),packageDetailsDto.getDependencies());
        return packageDetails;
    }

    public static PackageDetailsDto entityToDto(PackageDetails packageDetails) {
        PackageDetailsDto packageDetailsDto = new PackageDetailsDto(packageDetails.getPackageDetailsId(), packageDetails.getAuthorId(), packageDetails.getPackageDetailsName(),packageDetails.getPackageDetailsVersion(), packageDetails.getPackageLicense(), null, null, packageDetails.getDependencies(),packageDetails.getPackageDescription());
        return packageDetailsDto;
    }
}
