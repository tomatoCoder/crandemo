package com.cran.demo.dto;

public class PackageDetailsDto {

    private Integer packageDetailsId;

    private Integer authorId;

    private String packageDetailsName;

    private String packageDetailsVersion;

    private String packageLicense;

    private String authorName;

    private String authorEmail;

    private String dependencies;

    private String packageDescription;

    public PackageDetailsDto(Integer packageDetailsId, Integer authorId, String packageDetailsName, String packageDetailsVersion, String packageLicense, String authorName, String authorEmail, String dependencies, String packageDescription) {
        this.packageDetailsId = packageDetailsId;
        this.authorId = authorId;
        this.packageDetailsName = packageDetailsName;
        this.packageDetailsVersion = packageDetailsVersion;
        this.packageLicense = packageLicense;
        this.authorName = authorName;
        this.authorEmail = authorEmail;
        this.dependencies = dependencies;
        this.packageDescription = packageDescription;
    }

    public PackageDetailsDto(){

    }

    public Integer getPackageDetailsId() {
        return packageDetailsId;
    }

    public void setPackageDetailsId(Integer packageDetailsId) {
        this.packageDetailsId = packageDetailsId;
    }

    public Integer getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Integer authorId) {
        this.authorId = authorId;
    }

    public String getPackageDetailsName() {
        return packageDetailsName;
    }

    public void setPackageDetailsName(String packageDetailsName) {
        this.packageDetailsName = packageDetailsName;
    }

    public String getPackageDetailsVersion() {
        return packageDetailsVersion;
    }

    public void setPackageDetailsVersion(String packageDetailsVersion) {
        this.packageDetailsVersion = packageDetailsVersion;
    }

    public String getPackageLicense() {
        return packageLicense;
    }

    public void setPackageLicense(String packageLicense) {
        this.packageLicense = packageLicense;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getAuthorEmail() {
        return authorEmail;
    }

    public void setAuthorEmail(String authorEmail) {
        this.authorEmail = authorEmail;
    }

    public String getDependencies() {
        return dependencies;
    }

    public void setDependencies(String dependencies) {
        this.dependencies = dependencies;
    }

    public String getPackageDescription() {
        return packageDescription;
    }

    public void setPackageDescription(String packageDescription) {
        this.packageDescription = packageDescription;
    }
}
