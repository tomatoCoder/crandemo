package com.cran.demo.dto;

public class AuthorDto {
    private Integer authorId;

    private String authorEmail;

    private String authorName;

    public Integer getAuthorId() {
        return authorId;
    }

    public AuthorDto(String authorEmail, String authorName) {
        this.authorEmail = authorEmail;
        this.authorName = authorName;
    }

    public AuthorDto(){

    }

    public void setAuthorId(Integer authorId) {
        this.authorId = authorId;
    }

    public String getAuthorEmail() {
        return authorEmail;
    }

    public void setAuthorEmail(String authorEmail) {
        this.authorEmail = authorEmail;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }
}
