package com.cran.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CranDemoApplication {

	/*
	@Autowired
	PackageDetailsRepository packageDetailsRepository;
	*/

	public static void main(String[] args) {
		SpringApplication.run(CranDemoApplication.class, args);
	}

	/*
	@PostConstruct
	public void setupDbWithData(){
		User user= new User("Vikas", null);
		user.setSkills(Arrays.asList(new Skill("java"), new Skill("js")));
		user= userRepository.save(user);
	}*/
}
