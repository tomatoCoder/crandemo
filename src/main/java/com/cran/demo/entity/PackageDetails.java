package com.cran.demo.entity;

import javax.persistence.*;

@Entity
public class PackageDetails {

    @Column
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Integer packageDetailsId;

    @Column
    private String packageDetailsName;

    @Column
    private String packageDescription;

    @Column
    private String packageDetailsVersion;

    @Column
    private Integer authorId;

    @Column
    private String packageLicense;

    @Column
    private String dependencies;

    public PackageDetails(String packageDetailsName, String packageDescription, String packageDetailsVersion, Integer authorId, String packageLicense, String dependencies) {
        this.packageDetailsName = packageDetailsName;
        this.packageDescription = packageDescription;
        this.packageDetailsVersion = packageDetailsVersion;
        this.authorId = authorId;
        this.packageLicense = packageLicense;
        this.dependencies = dependencies;
    }

    public PackageDetails(){

    }

    public Integer getPackageDetailsId() {
        return packageDetailsId;
    }

    public void setPackageDetailsId(Integer packageDetailsId) {
        this.packageDetailsId = packageDetailsId;
    }

    public String getPackageDetailsName() {
        return packageDetailsName;
    }

    public void setPackageDetailsName(String packageDetailsName) {
        this.packageDetailsName = packageDetailsName;
    }

    public String getPackageDescription() {
        return packageDescription;
    }

    public void setPackageDescription(String packageDescription) {
        this.packageDescription = packageDescription;
    }

    public String getPackageDetailsVersion() {
        return packageDetailsVersion;
    }

    public void setPackageDetailsVersion(String packageDetailsVersion) {
        this.packageDetailsVersion = packageDetailsVersion;
    }

    public Integer getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Integer authorId) {
        this.authorId = authorId;
    }

    public String getPackageLicense() {
        return packageLicense;
    }

    public void setPackageLicense(String packageLicense) {
        this.packageLicense = packageLicense;
    }

    public String getDependencies() {
        return dependencies;
    }

    public void setDependencies(String dependencies) {
        this.dependencies = dependencies;
    }

}
