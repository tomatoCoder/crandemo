package com.cran.demo.entity;

import javax.persistence.*;

@Entity
public class Author {
    @Column
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer authorId;

    @Column
    private String authorEmail;

    @Column
    private String authorName;

    public Author(String authorEmail, String authorName) {
        this.authorEmail = authorEmail;
        this.authorName = authorName;
    }

    public Author(){

    }
    public Integer getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Integer authorId) {
        this.authorId = authorId;
    }

    public String getAuthorEmail() {
        return authorEmail;
    }

    public void setAuthorEmail(String authorEmail) {
        this.authorEmail = authorEmail;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }
}
