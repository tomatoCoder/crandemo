package com.cran.demo.repository;

import com.cran.demo.entity.PackageDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PackageDetailsRepository extends JpaRepository<PackageDetails, Integer> {

    PackageDetails findFirstByPackageDetailsVersion(String packageDetailsVersion);

    PackageDetails findFirstByPackageDetailsNameAndPackageDetailsVersion(String packageDetailsName, String packageDetailsVersion);

    PackageDetails findFirstByPackageDetailsName(String packageDetailsName);

    PackageDetails findFirstByPackageDetailsId(Integer packageDetailsId);

    List<PackageDetails> findAllByAuthorId(Integer authorId);
}
