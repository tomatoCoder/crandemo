package com.cran.demo.controller;

import com.cran.demo.dto.PackageDetailsDto;
import com.cran.demo.service.PackageDetailsService;
import com.cran.demo.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("/package")
@RestController
public class PackageDetailsController {

    @Autowired
    PackageDetailsService packageDetailsService;

    @RequestMapping(Constants.GET_PACKAGE_BY_ID)
    public PackageDetailsDto getPackageById(Integer packageDetailsId){
        return packageDetailsService.getPackageDetailsById(packageDetailsId);
    }

    @RequestMapping(Constants.GET_PACKAGE_BY_NAME)
    public PackageDetailsDto getPackageByName(String packageDetailsName){
        return packageDetailsService.getPackageDetailsByName(packageDetailsName);
    }

    @RequestMapping(value=Constants.SAVE_PACKAGE, method= RequestMethod.POST)
    public void savePackage(@RequestBody PackageDetailsDto packageDetailsDto) throws Exception {
        packageDetailsService.savePackage(packageDetailsDto);
    }

    @RequestMapping(Constants.GET_ALL_PACKAGES)
    public List<PackageDetailsDto> getAllPackages(){
        return packageDetailsService.getAllPackages();
    }

}
