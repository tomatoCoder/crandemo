package com.cran.demo.controller;

import com.cran.demo.dto.PackageDetailsDto;
import com.cran.demo.service.AuthorService;
import com.cran.demo.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("/author")
@RestController
public class AuthorController {

    @Autowired
    AuthorService authorService;

    //API to get authors by author name
    @RequestMapping(Constants.GET_PACKAGES_BY_AUTHOR)
    public List<PackageDetailsDto> getPackageByAuthor(@PathVariable String authorName){
        return authorService.findPackagesByAuthor(authorName);
    }
}
