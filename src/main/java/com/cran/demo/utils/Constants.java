package com.cran.demo.utils;

public interface Constants {
	static final String GET_PACKAGE_BY_ID = "/getPackage/{id}";
	static final String GET_PACKAGE_BY_NAME = "/getPackage/{name}";
	static final String SAVE_PACKAGE = "/savePackage";
	static final String GET_ALL_PACKAGES = "/getAllPackages";
	static final String GET_PACKAGES_BY_AUTHOR = "/getPackagesByAuthor/{authorName}";
}
