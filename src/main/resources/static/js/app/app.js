'use strict'

var demoApp = angular.module('demo', [ 'ui.bootstrap', 'demo.packageController',
		'demo.packageService' ]);
demoApp.constant("CONSTANTS", {
	getAllPackages : "/package/getAllPackages",
	getPackgesByAuthor: "/author/getPackagesByAuthor/"
});

/*
demoApp.config(function ($stateProvider, $urlRouterProvider) {
	
})*/