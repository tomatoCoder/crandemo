'use strict'

var module = angular.module('demo.packageController', []);
module.controller("PackageController", [ "$scope", "PackageService",
    function($scope, PackageService) {

        $scope.packageDetailsDto = {
            packageId : null,
            packageDetailsId: null,
            authorId: null,
            packageDetailsName: null,
            packageDetailsVersion: null,
            packageLicense: null,
            authorName: null,
            authorEmail: null,
            dependencies: null,
            packageDescription: null
        };

        $scope.author= null;
       $scope.searchbyauthor = function() {
           PackageService.getPackgesByAuthor($scope.author).then(function (value) {
               $scope.allPackages=value.data;
               console.log(value.data);
           }, function (reason) {
               console.log("error occured");
           }, function (value) {
               console.log("no callback");
           });
       }

        $scope.searchpackage = function() {
            PackageService.getAllPackages().then(function(value) {
                $scope.allPackages= value.data;
            }, function(reason) {
                console.log("error occured");
            }, function(value) {
                console.log("no callback");
            });
        }
    } ]);